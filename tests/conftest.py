from pathlib import Path
import pytest


test_dir = Path(__file__).parent
_sample_toml = test_dir / "sample.toml"


@pytest.fixture()
def sample_toml():
    return _sample_toml
