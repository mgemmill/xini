import xini


def test_flake8(sample_toml):
    flake8 = xini.INI_FILES["flake8"]
    assert flake8.toml_section_name == "tool.flake8"

    toml_cfg = xini.load_toml(sample_toml)
    print(toml_cfg)

    flake8.extract_config(toml_cfg)

    assert str(flake8) == (
        "[flake8]\n"
        "enable-extensions = G\n"
        "ignore = E501\n"
        "exclude = .git,__pycache__,build,dist,x_api.py\n"
        "show-source = true\n"
        "max-complexity = 10"
    )


def test_pytest(sample_toml):
    pytest = xini.INI_FILES["pytest"]
    assert pytest.toml_section_name == "tool.pytest"

    toml_cfg = xini.load_toml(sample_toml)
    print(toml_cfg)

    pytest.extract_config(toml_cfg)

    assert str(pytest) == (
        "[pytest]\n" "addopts = -vv --strict --cov=xini --cov-report=term-missing"
    )


def test_coverage(sample_toml):
    coverage = xini.INI_FILES["coverage"]
    assert coverage.toml_section_name == "tool.coverage"

    toml_cfg = xini.load_toml(sample_toml)
    print(toml_cfg)

    coverage.extract_config(toml_cfg)

    assert str(coverage) == (
        "[coverage]\n"
        "[run]\n"
        "source = xini\n"
        "branch = true\n"
        "[paths]\n"
        "source = src/xini\n"
        "[report]\n"
        "show_missing = true"
    )


def test_pylint(sample_toml):
    pylint = xini.INI_FILES["pylint"]
    assert pylint.toml_section_name == "tool.pylint"

    toml_cfg = xini.load_toml(sample_toml)
    print(toml_cfg)

    pylint.extract_config(toml_cfg)

    assert str(pylint) == (
        "[pylint]\n"
        "disable = apply-builtin,\n"
        "          attribute-defined-outside-init,\n"
        "          backtick,\n"
        "          bad-continuation,\n"
        "          bad-inline-option,\n"
        "          bad-python3-import,\n"
        "          basestring-builtin,\n"
        "          buffer-builtin,\n"
        "          cmp-builtin,\n"
        "          cmp-method,\n"
        "          coerce-builtin,\n"
        "          coerce-method,\n"
        "          comprehension-escape,\n"
        "          delslice-method,\n"
        "          deprecated-itertools-function,\n"
        "          deprecated-operator-function,\n"
        "          deprecated-pragma,\n"
        "          deprecated-str-translate-call,\n"
        "          deprecated-string-function,\n"
        "          deprecated-sys-function,\n"
        "          deprecated-types-field,\n"
        "          deprecated-urllib-function,\n"
        "          dict-items-not-iterating"
    )


def test_bogus_tool(sample_toml, capsys):
    pylint = xini.INIWriter("tool.bogus", ".bogusrc")
    assert pylint.toml_section_name == "tool.bogus"

    toml_cfg = xini.load_toml(sample_toml)
    pylint.extract_config(toml_cfg)
    out, err = capsys.readouterr()

    assert out == (
        "Searching for `tool.bogus` section...\n" "    [tool.bogus] does not exist.\n"
    )
