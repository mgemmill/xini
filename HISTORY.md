### xini


v0.2.1 (2019-01-20)
-------------------

* Added pylint extraction
* Bumped test coverage to 80%


v0.1.2 (2019-01-20)
-------------------

* Graceful exit if no pyproject.toml file found.
